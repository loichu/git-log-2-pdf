# git-log-2-pdf

Project to create a tool that can generate a report/work diary based on git-log.


# Keywords
gitlog2pdf, git2pdf, git2report, git2workdiary, etc...


# General idea
One want to be able to give his manager a report of his day's activities. For 
that, he just runs a cmd in his git repository to create a report of the project
 that can contain:
  - Project description
  - General statistics
  - Detailed commit messages of the day or week
  - A commit graph in SVG
  - Older commit messages

## Search
* https://stackoverflow.com/questions/6489088/generate-a-pdf-logbook-from-git-commits
    * http://www.importsoul.net/python/pygitbook/
    * https://github.com/Hugoagogo/pyGitBook
* https://stackoverflow.com/questions/1057564/pretty-git-branch-graphs
* https://github.com/Hightor/gitlog
    * http://mirrors.ibiblio.org/CTAN/macros/latex/contrib/gitlog/gitlog.pdf
* https://coderwall.com/p/bcthew/create-your-work-report-using-git-log
* http://marklodato.github.io/visual-git-guide/index-en.html?no-svg

## Graphs
* https://stackoverflow.com/questions/1838873/visualizing-branch-topology-in-git
* http://bit-booster.com/best.html
* http://gitgraphjs.com/
* https://github.com/esc/git-big-picture

## Icons
* https://icons8.com/icons/set/git-branch

## Statistics
* `git shortlog -s -n`
* https://stackoverflow.com/questions/1828874/generating-statistics-from-git-repository

